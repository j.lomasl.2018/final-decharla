# Final DeCharla

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Jesus Lomas Lopez
* Titulación: Grado en Tecnologías de Telecomunicaciones
* Cuenta en laboratorios: jesuslom
* Cuenta URJC: j.lomasl.2018
* Video básico (url): https://youtu.be/smguLPnGB8A
* Video parte opcional (url): https://youtu.be/1n8WiPQoZzE
* Despliegue (url): https://jesuslom.pythonanywhere.com/
* Contraseñas: jesus1 / lomas2
* Cuenta Admin Site: jesuslom/qwerty2023

## Resumen parte obligatoria
En esta práctica final se ha utilizado django para generar una aplicación que sirva para intercambiar y leer tanto mensajes 
como imágenes.
Nada más acceder, te pedirán que introduzcas una contraseña para entrar a dicha aplicación (están escritas más arriba), sin
registrarse no se te permitirá el acceso. Una vez cumplido ese primer paso, aparecerá una página en la que se puede ver 
un menú con las distintas partes:
- DeCharla --> es la propia página principal en la que se pueden observar las distintas salas creadas por el usuario, además
aparece un botón que te permite buscar el nombre de las salas y poder entrar a participar en ellas, además puedes crear una sala
nueva, para ello debes escribir su nombre en el formulario que aparece y pinchar en el botón.
Por último, aparece abajo de la página un resumen con la información de las salas.
- Ayuda --> es una página destinada a explicar el funcionamiento de la aplicación.
- Configuration --> te permite cambiar el nombre de usuario, ya que al iniciar sesión con una de las dos contraseñas se te asigna 
el nombre de usuario "anónimo". También puedes cambiar el tipo de fuente y el tamaño de la letra.
- Admin --> te redirige a una página destinada al control de la base de datos.

Se crean test extremo a extremo.

Cabe destacar también que si escribimos en la url /din una vez estemos dentro de una sala se ejecutará un javascript el cual 
actualizará la página cada 30 segundos en busca de mensajes nuevos. A su vez, si dentro de una sala escribimos en la url /json
nos redirige a una nueva página la cual nos muestra los mensajes de las salas en formato json
## Lista partes opcionales

* Favicon 
* Cerrar sesión
* Me gusta a las salas

