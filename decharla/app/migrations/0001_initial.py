# Generated by Django 4.1.7 on 2023-06-26 15:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Password",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("Password", models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name="Sesion",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("Sesion", models.TextField(unique=True)),
                ("Nombre", models.TextField(default="anonimo")),
                ("font_size", models.CharField(default="small", max_length=10)),
                ("font_type", models.CharField(default="sans-serif", max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name="Sala",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("Nombre", models.CharField(max_length=64)),
                ("Votos", models.PositiveIntegerField(default=0)),
                (
                    "Autor",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="app.sesion"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Mensaje",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("Fecha", models.DateTimeField(auto_now_add=True)),
                ("Texto", models.TextField()),
                ("is_image2", models.BooleanField(default=False)),
                ("imagenURL", models.URLField(blank=True, null=True)),
                (
                    "Autor",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="app.sesion"
                    ),
                ),
                (
                    "Sala",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="app.sala"
                    ),
                ),
            ],
        ),
    ]
