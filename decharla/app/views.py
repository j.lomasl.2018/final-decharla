from unittest import loader

from django.views.decorators.csrf import ensure_csrf_cookie
from django.core import validators
from django.shortcuts import render, HttpResponseRedirect, redirect
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.views.decorators.csrf import csrf_exempt
from .models import Sesion, Sala, Mensaje, Password
from django.http import HttpResponse
from django.utils import timezone
import xml.etree.ElementTree as ET
import random, string, json
import os
import datetime
from django.conf import settings
from django.conf.urls.static import static

#####################################################################################################################################################################


def poner_favicon(request):
    favicon_path = os.path.join(settings.STATIC_ROOT, 'image/favicon.png')
    with open(favicon_path, 'rb') as favicon:
        respuesta = HttpResponse(favicon.read(), content_type="image/png")
    return respuesta
#####################################################################################################################################################################


def xml_parser(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        es_imagen = msg.get('es_imagen')
        if es_imagen == "true":
            es_imagen_aux = True
        else:
            es_imagen_aux = False
        text = msg.find('text').text
        messages.append({'es_imagen': es_imagen_aux, 'text': text})
    return messages

#####################################################################################################################################################################


def info_guardada():
    salas_activas = Sala.objects.count()
    mensajes = Mensaje.objects.filter(is_image2=False).count() #is_image=False porque sino lo detecta como imagen
    imagenes = Mensaje.objects.filter(is_image2=True).count()
    return salas_activas, mensajes, imagenes
#####################################################################################################################################################################


def comprobar_sesion(request):
    cookie_sesion = request.COOKIES.get("Sesion") #mira la cookie asociada a la sesion
    if cookie_sesion is None: #Si no tengo la cookkie guardada creo una asociada a la nueva sesion
        cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=128))
        response = HttpResponseRedirect("/login")
        response.set_cookie("Sesion", cookie) #establezco la cookie a la sesion
        return response
    if request.path != "/login": #Si existe la cookie
        check = Sesion.objects.filter(Sesion=cookie_sesion).exists() #compruebo que tengo la cookie guardada asociada a la sesion
    else:
        check = True
    if not check: #si no existe cookie en la base de datos reedirijo al login
        return HttpResponseRedirect("/login")
    return None

#####################################################################################################################################################################


@csrf_exempt
def index(request):
    sesion_ = comprobar_sesion(request) #compruebo la sesion del usuario
    if sesion_:
        return sesion_
    #si tengo esa sesion -->
    if request.method == "POST" or request.method == "GET":
        try:
            nombre = request.POST.get("sala")
            # obtengo el valor de 'sala' y compruebo si esta existe una sala asociada a ese nombre en la base de datos
            if not Sala.objects.filter(Nombre=nombre).exists():
                # si no la tengo, obtendre el objeto sesion asociado a la cookie de la sesion activa
                sesion_nueva = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
                sala_nueva = Sala(Nombre=nombre, Autor=sesion_nueva) #a la sala nueva asocio nombre de la sala y del chateador
                sala_nueva.save()
                return HttpResponseRedirect("/" + nombre)
        except Exception as e:
            print("Error:", str(e))
         #   return render(request, "error.html")
    salas_activas, mensajes, imagenes = info_guardada() #reuno los datos del chat total
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #tengo el objeto de la sesion de la cookie de sesion actual
    if sesion.Nombre:
        sesion_nombre = sesion.Nombre
    else:
        sesion_nombre = "Anonimo"
    context = { "lista_sala": lista_salas(request), "salas_activas": salas_activas, "mensajes": mensajes, "imagenes": imagenes,
                "sesion": sesion, "user_name": sesion_nombre}
    return render(request, "pagina_inicio.html", context)

#####################################################################################################################################################################


@csrf_exempt
def registrar(request):
    try:
        if request.method == "GET" or request.method == "POST":
            contrasena = request.POST.get("password") #obtengo la contraseña introducida
            if Password.objects.filter(Password=contrasena).exists(): #si la contraseña esta ya en la base de datos
                # asocio esa sesion a la cookie de la sesion guardada
                sesion = Sesion()
                sesion.Sesion = request.COOKIES.get("Sesion")
                sesion.save()
                return HttpResponseRedirect("/") #reedirijo a la pag_inicio
        #compruebo el valor de la cookie de sesion de la solicitud
        cookie = request.COOKIES.get("Sesion")
        if Sesion.objects.filter(Sesion=cookie).exists(): #compruebo si existe con ese valor de cookie de sesion
            return HttpResponseRedirect("/")
    except Sesion.DoesNotExist: #no se da ningun caso --> error
        return HttpResponse(status=404)
    return render(request, "registro.html")
#####################################################################################################################################################################


#FUNCIONALIDAD OPTATIVA - permite cerrar las sesiones de usuario autenticados
@csrf_exempt
def cerrar_sesion(request):
    if request.method == "POST":
        sesion_cookie = request.POST.get("cookie") #obtengo valor de la cookie de sesion
        if Sesion.objects.filter(Sesion=sesion_cookie).exists(): #compruebo que la cookie de esa sesion existe
            sesion = Sesion.objects.get(Sesion=sesion_cookie)
            sesion.delete() #la elimino y redirecciono a la pag_inicio
        redireccion = HttpResponseRedirect("/")
        redireccion.delete_cookie("Sesion")
        return redireccion

#####################################################################################################################################################################


@csrf_exempt
def configuracion(request):
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
    if not sesion:
        return render("registro.html", request)
    if request.method == "POST":
        nombre = request.POST.get("name")
        sesion.Nombre = nombre
        font_size = request.POST.get("font_size")
        sesion.font_size = font_size
        font_type = request.POST.get("font_type")
        sesion.font_type = font_type
        sesion.save()
    salas_activas, mensajes, imagenes = info_guardada()
    configuration_html = render(request, "pagina_configuracion.html", { "salas_activas": salas_activas,"mensajes": mensajes,"sesion": sesion,
                                                                        "imagenes": imagenes, "lista_salas": lista_salas(request)})
    return configuration_html


#####################################################################################################################################################################


def seccion_ayuda(request):
    if request.method == "GET" or request.method == "POST":
        sesion_ = comprobar_sesion(request) #comrpobar la sesion de usuario
        if sesion_:
            return sesion_
        sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #obtengo el objeto sesion, utiliza el valor de la cookie
                                                                    #llamada "sesion" y este tiene que coincidir con dicho valor
        salas_activas, mensajes, imagenes= info_guardada()
        sala_lista = lista_salas(request)
        pagina_ayuda = render(request, "pagina_ayuda.html",{"lista_salas": sala_lista, "sesion": sesion, "salas_activas": salas_activas,
                                "mensajes": mensajes, "imagenes": imagenes})
        return pagina_ayuda

#####################################################################################################################################################################


def lista_salas(request):
    sesion_ = comprobar_sesion(request) #comrpobar la sesion de usuario
    if sesion_:
        return sesion_
    lista_salas = Sala.objects.all() #obtengo las salas almacenadas en la base de datos
    for sala_ in lista_salas:
        mensajes = Mensaje.objects.filter(Sala=sala_).count() #obtengo el numero de mensajes asociados a la sala actual
        ultima_visita = request.session.get(f"ultima_visita{sala_.Nombre}") #obtengo el valor de la ultima visita
        if ultima_visita:
            if isinstance(ultima_visita, str): #si obtengo valor en ultima visita
                ultima_visita = datetime.datetime.strptime(ultima_visita, "%Y-%m-%d %H:%M:%S")  # Convertir a datetime
            ultimos_mensajes_vistos = Mensaje.objects.filter(Sala=sala_, Fecha__gt=ultima_visita).exclude(Autor__Sesion=request.COOKIES.get("Sesion")).count()
            #esta ultima linea cuenta el num de mensajes enviados
        else:
            ultimos_mensajes_vistos = mensajes #guardo lo anterior en mensajes
        sala_.mensajes = mensajes
        sala_.ultimos_mensajes_vistos = ultimos_mensajes_vistos
        request.session[f"ultima_visita_{sala_.Nombre}"] = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
    return lista_salas
####################################################################################################################################################################


@ensure_csrf_cookie
def html_listado_salas(request):
    sesion_ = comprobar_sesion(request) #comrpobar la sesion de usuario
    if sesion_:
        return sesion_
    sala_no_existe = False
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #obtengo el objeto sesion, utiliza el valor de la cookie
                                                                    #llamada "sesion" y este tiene que coincidir con dicho valor
    if request.method == "POST":
        sala_nombre = request.POST.get("sala_nombre") #obtengo valor sala_nombre
        if sala_nombre:#se verifica si tiene un valor
            try: #compruebo que ese nombre coincide con sala en la base de datos
                sala = Sala.objects.get(Nombre=sala_nombre)
                return HttpResponseRedirect(f'/{sala.Nombre}')
            except Sala.DoesNotExist:
                sala_no_existe = True
    salas_activas, mensajes, imagenes = info_guardada()
    return render(request, "listado_salas.html",{"sala_no_existe": sala_no_existe, "sesion": sesion, "salas_activas": salas_activas, "mensajes": mensajes, "imagenes": imagenes})
####################################################################################################################################################################


@csrf_exempt
def crear_sala_nueva(request, id_sala):
    sala_ = comprobar_sesion(request) #comrpobar la sesion de usuario
    if sala_:
        return sala_
    redireccion = HttpResponseRedirect("/login")
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
    try: #busco si en la base de datos tengo algún objeto de sala que coincida con el nombre_sala
        nombre_sala = Sala.objects.get(Nombre=id_sala)
        mensajes = Mensaje.objects.filter(Sala=nombre_sala).order_by('-Fecha')
        if request.method == "POST":
            texto = request.POST.get("mensaje")
            es_imagen2 = bool(request.POST.get('is_image2', False)) #compruebo si es imagen
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion")) #obtiene sesion actual o crea una nueva
            mensaje = Mensaje(Texto=texto, is_image2=es_imagen2, Autor=autor, Sala=nombre_sala) #crea nuevo mensaje con los datos
            if es_imagen2:
                if texto: #compruebo si tengo el valor para el campo mensaje
                    mensaje.imagenURL = texto
                    try:
                        validators.URLValidator()(mensaje.imagenURL)
                    except ValidationError:
                        return HttpResponse(status=404)
            mensaje.save()
        if request.method == "PUT":
            xml_string = request.body.decode('utf-8')
            xml_messages = xml_parser(xml_string) #analiza xml y obtiene mensajes
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))
            for msg in xml_messages:
                mensaje = Mensaje(Texto=msg['text'], is_image2=msg['es_imagen'], Autor=autor, Sala=nombre_sala)
                mensaje.imagenURL = msg['text']
                mensaje.save()
        salas_activas, mensajes2, imagenes = info_guardada()
        sala_nueva = render (request, "sala_nueva.html", {"id_sala":id_sala, "salas_activas": salas_activas, "sesion":sesion, "mensajes": mensajes, "imagenes":imagenes, "lista_salas": lista_salas(request)})
        return sala_nueva
    except Sala.DoesNotExist:
        return redireccion
####################################################################################################################################################################


@csrf_exempt
def sala_dinamica(request, id_sala):
    sala_ = comprobar_sesion(request) #comrpobar la sesion de usuario
    if sala_:
        return sala_
    redireccion = HttpResponseRedirect("/login")
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
    try:
        nombre_sala = Sala.objects.get(Nombre=id_sala)
        mensajes = Mensaje.objects.filter(Sala=nombre_sala).order_by('-Fecha')
        if request.method == "POST": #se obtienen los datos del formulario y se crea un nuevo mensaje
            texto = request.POST.get("mensaje")
            is_image2 = bool(request.POST.get('is_image2', False))
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion")) #obtiene sesion actual o crea una nueva
            mensaje = Mensaje(Texto=texto, is_image2=is_image2, Autor=autor, Sala=nombre_sala) #crea nuevo mensaje con los datos
            if is_image2: #si es una imagen, se valida y asigna url, en caso de no valer, salta error
                if texto:
                    mensaje.imagenURL = texto
                    try:
                        validators.URLValidator()(mensaje.imagenURL)
                    except ValidationError:
                        return HttpResponse(status=404)
            mensaje.save() #guarda el mensaje
        sala_nueva = render (request, "sala_dinamica.html", {"id_sala":id_sala, "sesion":sesion, "mensajes": mensajes, "lista_salas": lista_salas(request)})
        return sala_nueva #se utiliza la plantilla html
    except Sala.DoesNotExist:
        return redireccion
####################################################################################################################################################################


def sala_json(request, id_sala):
    response = comprobar_sesion(request) #compruebo la sesion del usuario
    if response:
        return response
    try:
        sala_ = Sala.objects.get(Nombre=id_sala)
        mensajes = Mensaje.objects.filter(Sala=sala_).order_by('-Fecha') #obtengo los Mensajes filtrando sala y ordenados por fecha descendente
        json_mensajes = [] #lista vacia para almacenar
        for mensaje in mensajes: #se itera y crea un diccionario esa info
            info_mensaje = {"autor": mensaje.Autor.Nombre, "text": mensaje.Texto, "es_imagen": mensaje.is_image2, "fecha": mensaje.Fecha.strftime("%Y-%m-%d %H:%M:%S")}
            json_mensajes.append(info_mensaje)
        return HttpResponse(json.dumps(json_mensajes), content_type="application/json")
    except Sala.DoesNotExist: #caso de no existir la sala, se da un error 404
        return HttpResponse(status=404)

####################################################################################################################################################################


@csrf_exempt
def borrar_sala(request, id_sala):
    try:
        sesion_ = comprobar_sesion(request) #compruebo la sesion del usuario
        if sesion_:
            return sesion_
        sala = Sala.objects.get(id=id_sala)
        sala.delete() #se busca el objeto sala del id_sala y lo elimino
        return HttpResponseRedirect("/")
    except ObjectDoesNotExist:
        return HttpResponseRedirect("/")

####################################################################################################################################################################
#FUNCIONALIDAD OPTATIVA - permite dar me gusta a las salas


def me_gusta(request, id_sala):
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #obtengo el objeto sesion, utiliza el valor de la cookie
                                                                    #llamada "sesion" y este tiene que coincidir con dicho valor
    if sesion: #se verifica si se encontro sesion
        try: #se busca el objeto sala de la id_sala para sumar los me gusta
            sala = Sala.objects.get(id=id_sala)
            sala.Votos += 1
            sala.save()
        except Exception as e:
            print("Error:", str(e))
    return HttpResponseRedirect("/")