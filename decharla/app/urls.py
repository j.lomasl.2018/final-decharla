from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.index, name='principal'),
    path('login', views.registrar, name='login'),
    path('logout/', views.cerrar_sesion, name='logout'),
    path('configuracion', views.configuracion, name='configuracion'),
    path('ayuda', views.seccion_ayuda, name='ayuda'),
    path('busca_paginas', views.html_listado_salas, name='busca_paginas'),
    path('<str:id_sala>', views.crear_sala_nueva, name='sala'),
    path('<str:id_sala>/din', views.sala_dinamica, name='saladin'),
    path('<str:id_sala>/json', views.sala_json, name='sala_json'),
    path('borrar/<int:id_sala>', views.borrar_sala, name='borrar_sala'),
    path('votar/<int:id_sala>', views.me_gusta, name='votar'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,
                    document_root=settings.MEDIA_ROOT)


