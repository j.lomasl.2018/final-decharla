from django.test import TestCase

# Create your tests here.


import json

from django.http import HttpResponseBadRequest
from django.test import TestCase, Client
from django.urls import reverse
from .models import Sesion, Sala, Mensaje, Password  # Asegúrate de importar tus modelos correctamente
from django.core.files.uploadedfile import SimpleUploadedFile


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.sesion = Sesion.objects.create(Sesion="SesionTest", Nombre='testsesion')
        self.sala = Sala.objects.create(Nombre='testsala', Autor=self.sesion)
        self.mensaje = Mensaje.objects.create(Texto='testmessage', is_image=False, Autor=self.sesion, Sala=self.sala)
        self.password = Password.objects.create(Password='testpassword')

    def test_login_POST(self):
        response = self.client.post(reverse('login'), {
            'password': 'testpassword'
        })

        self.assertEqual(response.status_code, 302)

    def test_principal_GET(self):
        self.client.cookies["Sesion"] = self.sesion.Sesion
        response = self.client.get(reverse('principal'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'principal.html')

    def test_sala_GET(self):
        self.client.cookies["Sesion"] = self.sesion.Sesion
        response = self.client.get(reverse('sala', args=[self.sala.Nombre]))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'sala.html')

    def test_ayuda_GET(self):
        self.client.cookies["Sesion"] = self.sesion.Sesion
        response = self.client.get(reverse('ayuda'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pagina_ayuda.html')

    def test_configuracion_POST(self):
        self.client.cookies["Sesion"] = self.sesion.Sesion
        response = self.client.post(reverse('configuracion'), {
            'name': 'newname',
            'font_size': '16',
            'font_type': 'Arial'
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'configuracion.html')

    def test_busca_paginas_POST(self):
        self.client.cookies["Sesion"] = self.sesion.Sesion
        response = self.client.post(reverse('busca_paginas'), {
            'sala_nombre': self.sala.Nombre
        })

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, f'/{self.sala.Nombre}')

    def test_votar_GET(self):
        self.client.cookies["Sesion"] = self.sesion.Sesion
        response = self.client.get(reverse('votar', args=[self.sala.id]))

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/')


